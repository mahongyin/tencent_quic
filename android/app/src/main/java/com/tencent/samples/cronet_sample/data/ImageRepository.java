/*
 * Copyright (C) 2016 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.tencent.samples.cronet_sample.data;

public class ImageRepository {
    private static String[] imageUrls= {
            "https://s3.bmp.ovh/imgs/2021/12/e75badc219cb8024.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/8572a9af4e324eed.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/50d8b96185e2182c.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/a34620a924f85b5f.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/34f032435f7a3829.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/40cd58f25309e221.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/03d86c10c50447a5.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/f5b8c6d3636300c7.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/99ce27b1702eac4c.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/c248e4ab77979cc8.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/34f032435f7a3829.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/40cd58f25309e221.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/03d86c10c50447a5.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/f5b8c6d3636300c7.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/99ce27b1702eac4c.jpg",
            "https://s3.bmp.ovh/imgs/2021/12/c248e4ab77979cc8.jpg"
    };

    public static int numberOfImages() {
        return imageUrls.length;
    }

    public static String getImage(int position) {
        return imageUrls[position];
    }
}
